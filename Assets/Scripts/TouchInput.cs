﻿using UnityEngine;
using System.Collections;

public class TouchInput : MonoBehaviour {

	private Touch initialTouch = new Touch();
	public float swipeDistance = 0;
	public float triggerDistance = 200f;
	public bool hasSwiped = false;

	// Update is called once per frame
	void FixedUpdate() {

		foreach (Touch touch in Input.touches) {
			//left hand side input
			if (touch.position.x < Screen.height / 2) {

				Vector3 swipeDirection = new Vector3();

				if (touch.phase == TouchPhase.Began) {
					initialTouch = touch;
				}
				else if (touch.phase == TouchPhase.Moved) {
					float deltaX = initialTouch.position.x - touch.position.x;
					float deltaY = initialTouch.position.y - touch.position.y;

					//returns the relative distance of 
					swipeDistance = Mathf.Sqrt(Mathf.Pow(deltaX, 2) + Mathf.Pow(deltaY, 2));



					if (swipeDistance > triggerDistance) {
						//being exceptionally clever with working out which direction the swipe was in.
						if (deltaX > deltaY) {
							swipeDirection.x = deltaX / Mathf.Abs(deltaX);
						}
						else {
							swipeDirection.y = deltaY / Mathf.Abs(deltaY);
						}
					}
					hasSwiped = true;
				}
				else if (touch.phase == TouchPhase.Ended) {

					GameObject.Find("Player").GetComponent<Controller>().move(swipeDirection);
					initialTouch = touch;
					hasSwiped = false;
				}
			}


		}
	}
}
