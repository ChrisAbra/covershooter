﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {
	//seralised class which denotes a max and minimum
	[Serializable]
	public class Count {
		public int minimum;
		public int maximum;

		public Count(int min, int max) {
			minimum = min;
			maximum = max;
		}
	}

	//start level is set in Inspector and sets level on Awake
	public int startLevel;
	private int level;
	public int levelModifer = 1;

	//defines the number of columns and rows for the map
	public int columns = 8;
	public int rows = 8;
	//defines the number of walls to spawn on EACH side - random between the max and min.
	public Count wallCount = new Count(5, 9);


	//defines the relevant tiles to be added to the floor and wall sorting layers
	public GameObject[] floorTiles;
	public GameObject[] playerWallTiles;
	public GameObject[] enemyWallTiles;
	public GameObject[] outerWallTiles;

	//holds the Player prefab reference 
	public GameObject playerPrefab;
	//holds an array of any and all enemy types.
	public GameObject[] enemies;

	//an object to hold all the elements spawned in the hierachy. 
	private Transform boardHolder;
	private Transform enemyHolder;
	private Transform Player;


	//a divided list of player's possible possitions, and enemies' possible positions.
	private List<Vector3> playerGridPositions = new List<Vector3>();
	private List<Vector3> enemyGridPositions = new List<Vector3>();
	//holds references to the players walls
	private List<Vector3> playerWalls = new List<Vector3>();


	//initialises the list of positions for each side
	void InitialiseLists() {
		playerGridPositions.Clear();
		enemyGridPositions.Clear();


		//columns/2 is half of the values (FLOOR DIVISION), and the initial value of 0 goes all the way to the edge. to leave a ring of floor, +1 to min, -1 to max
		for (int x = 0; x < columns / 2; x++) {
			for (int y = 0; y < rows; y++) {
				playerGridPositions.Add(new Vector3(x, y, 0f));
			}
		}
		for (int x = (columns / 2) + 1; x < columns; x++) {
			for (int y = 0; y < rows; y++) {
				enemyGridPositions.Add(new Vector3(x, y, 0f));
			}
		}
	}

	//sets up the board with floor tiles and any border tiles if not commented out
	void BoardSetup() {

		for (int x = -1; x < columns + 1; x++) {
			for (int y = 0; y < rows; y++) {
				GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];


				/*
				if (x == -1 || x == columns || y == -1 || y == rows)
					toInstantiate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];
				*/

				GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
				instance.transform.SetParent(boardHolder);
			}

		}
	}
	//returns a random position on the grid provided (either playerSide or enemySide)
	//removes each position from the list each time it is taken per function call
	List<Vector3> RandomPositions(List<Vector3> gridPositions, int numPositions) {
		List<Vector3> randomPositions = new List<Vector3>();
		for (int i = 0; i < numPositions; i++) {
			int randomIndex = Random.Range(0, gridPositions.Count);
			Vector3 randomPosition = gridPositions[randomIndex];
			gridPositions.RemoveAt(randomIndex);
			randomPositions.Add(randomPosition);

		}

		return randomPositions;
	}



	//places items randomly on either the player space or the enemy space. If more than one object is passed in the array then it will select randomly from the objects at each position and will not repeat
	void LayoutObjectsAtRandom(GameObject[] objectArray, int objectCount, bool playerSpace) {
		List<Vector3> positionSpace = new List<Vector3>();
		//determines whether it is the left or right side (player or enemy grid) to place objects on.
		if (playerSpace == true) { positionSpace = playerGridPositions; }
		else { positionSpace = enemyGridPositions; }

		List<Vector3> randomPositions = RandomPositions(positionSpace, objectCount);

		for (int i = 0; i < objectCount; i++) {
			GameObject tileChoice = objectArray[Random.Range(0, objectArray.Length)];
			GameObject instance = Instantiate(tileChoice, randomPositions[i], Quaternion.identity) as GameObject;
			instance.transform.SetParent(boardHolder);
		}
	}

	//lays out objects from the tile array provided at the load of the level, taking the Count class of the corresponding object provided - for walls and init enemies and player 

	//initalises the number of Enemies as levels * the level modifier (currently default to 1).
	void LayoutStartAtRandom(GameObject[] tileArray, int minimum, int maximum, bool playerSpace) {
		int objectCount = Random.Range(minimum, maximum + 1);
		//enemy in the array select from random or pass value

		int enemyNumber = 0;

		List<Vector3> positionSpace = new List<Vector3>();

		//determines whether it is the left or right side (player or enemy grid) to place objects on.
		if (playerSpace == true) { positionSpace = playerGridPositions; }
		else { positionSpace = enemyGridPositions; }
		//gets a list of unique random positions from the determined space
		List<Vector3> randomPositions = RandomPositions(positionSpace, objectCount);
		//places the number of objects provided to the function
		for (int i = 0; i < objectCount; i++) {
			//instantiates each one at the position with the index in the object array
			GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
			GameObject instance = Instantiate(tileChoice, randomPositions[i], Quaternion.identity) as GameObject;
			instance.transform.SetParent(boardHolder);
			//if the required number of enemies haven't been spawned yet, spawn another
			if (playerSpace == false && enemyNumber < level * levelModifer) {
				SpawnEnemy(0, randomPositions[i]);
				enemyNumber += 1;
			}
			//if placing player walls, then keep a list of them to check for the leftmost one next
			if (playerSpace == true) {
				playerWalls.Add(randomPositions[i]);
			}
		}

	}
	//decides where to place the player on init
	void placePlayer() {
		//initialises to the top right most position and compares from there
		Vector3 currentLowestPosition = new Vector3(columns, rows, 0f);
		//loops each value in the player's walls
		foreach (Vector3 value in playerWalls) {
			//whichever value has the lowest x position is set and compared on the next loop
			if (value.x < currentLowestPosition.x) {
				currentLowestPosition = value;
				currentLowestPosition.y = rows / 2;
			}
		}
		//spawns the player at the determined leftmost position
		SpawnPlayer(currentLowestPosition);

	}


	//Spawns the enemy of a given type at a given position
	void SpawnEnemy(int enemyType, Vector3 position) {
		GameObject enemyInstance = Instantiate(enemies[enemyType], position, Quaternion.identity) as GameObject;
		enemyInstance.transform.SetParent(enemyHolder);
	}

	//Spawns the player at a given position - only use on awake (i.e. once per game), otherwise use the teleport function on the Player Class
	void SpawnPlayer(Vector3 position) {
		if (GameObject.Find("Player") == null) {
			GameObject playerInstance;
			playerInstance = Instantiate(playerPrefab, position, Quaternion.identity) as GameObject;
			playerInstance.name = "Player";
		}
	}


	//Sets up the board with initalised Walls, size, enemies and player
	public void SetupScene(int level) {
		boardHolder = new GameObject("Board").transform;
		enemyHolder = new GameObject("Enemies").transform;

		BoardSetup();
		InitialiseLists();
		LayoutStartAtRandom(playerWallTiles, wallCount.minimum, wallCount.maximum, true);
		LayoutStartAtRandom(enemyWallTiles, wallCount.minimum, wallCount.maximum, false);

		placePlayer();
		GameObject.Find("Main Camera").transform.SetParent(GameObject.Find("Player").transform, false);

		GameObject.Find("Main Camera").transform.position.y.Equals(2);
	}

	void Awake() {
		//sets the private internal level from the starting level in 
		level = startLevel;
		SetupScene(level);
	}



}
