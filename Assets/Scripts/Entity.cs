﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {

	public void move(Vector3 direction) {

		//if bounds the entity within the board.
		if(transform.position.x + direction.x >= 0 && transform.position.x + direction.x < GameObject.Find("GameManager").GetComponent<BoardManager>().columns) {
			if (transform.position.y + direction.y >= 0 && transform.position.y + direction.y < GameObject.Find("GameManager").GetComponent<BoardManager>().rows) {
				//updates the position based on the one provided
				transform.position += direction;
			}
		}

	}
}

