﻿using UnityEngine;
using System.Collections;

public class Controller : Player {

	void Update() { 

		if (Input.anyKeyDown){
			Vector3 movementDirection = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0f);
			move(movementDirection);
		}
	}
}
